package com.wiredbraincoffee.demos;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;

public class XmlJacksonMapper2 {
    public static void main(String[] args) {
        try {
            ObjectMapper mapper = new XmlMapper();
            UserData userData = mapper.readValue(
              new File("demo-files/user-data.xml"),
              UserData.class
            );
            System.out.println(userData);
            System.out.println(userData.getFirstName() + " " + userData.getLastName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
