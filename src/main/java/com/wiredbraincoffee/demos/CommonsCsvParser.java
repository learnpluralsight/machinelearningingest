package com.wiredbraincoffee.demos;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class CommonsCsvParser {

    public static void main(String[] args) {
        // try-with-resource makes sure the stream is closed at the end of the block
        try (Reader in = new FileReader("demo-files/sales-june-2020.csv")) {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.parse(in);
            for (CSVRecord record : records) {
                System.out.print(record.get(0) + ":" + record.get(1));
                System.out.print("," + record.get(2)+ ",");
                System.out.print(record.get(3));
                System.out.print("x");
                System.out.println(record.get(4));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
