package com.wiredbraincoffee.demos;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class JacksonMapper {
    public static void main(String[] args) {
        try {
            ObjectMapper mapper = new JsonMapper();
            Map<String, Object> userData = mapper.readValue(
              new File("demo-files/user-data.json"),
              Map.class
            );
            for (Map.Entry<String,Object> keyValue : userData.entrySet()) {
                System.out.println(keyValue.getKey() + "(" + keyValue.getValue().getClass().getSimpleName() + ") => " + keyValue.getValue());
            }
            System.out.println(userData);
            System.out.println(userData.get("name") + " " + userData.get("surname"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
