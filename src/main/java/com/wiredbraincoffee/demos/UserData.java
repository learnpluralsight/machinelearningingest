package com.wiredbraincoffee.demos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;
import java.util.Objects;

public class UserData {
    @JsonProperty("name")
    private String firstName;
    @JsonProperty("surname")
    private String lastName;
    private int totalSales;
    @JacksonXmlElementWrapper(localName = "salesAreas")
    @JacksonXmlProperty(localName = "salesArea")
    private List<Integer> salesArea;
    private boolean supervisor;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(int totalSales) {
        this.totalSales = totalSales;
    }

    public List<Integer> getSalesArea() {
        return salesArea;
    }

    public void setSalesArea(List<Integer> salesArea) {
        this.salesArea = salesArea;
    }

    public boolean isSupervisor() {
        return supervisor;
    }

    public void setSupervisor(boolean supervisor) {
        this.supervisor = supervisor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserData userData = (UserData) o;
        return firstName.equals(userData.firstName) &&
                lastName.equals(userData.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName);
    }

    @Override
    public String toString() {
        return "UserData{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", totalSales=" + totalSales +
                ", salesArea=" + salesArea +
                ", supervisor=" + supervisor +
                '}';
    }
}
