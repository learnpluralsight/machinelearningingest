package com.wiredbraincoffee.demos;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class CommonsCsvParser2 {

    public static void main(String[] args) {
        // try-with-resource makes sure the stream is closed at the end of the block
        try (Reader in = new FileReader("demo-files/sales-june-2020.csv")) {
//            Iterable<CSVRecord> records_example = CSVFormat.DEFAULT
//                    .withSkipHeaderRecord()
//                    .withDelimiter('\t')
//                    .withIgnoreEmptyLines(true)
//                    .withHeader(
//                      "id","time","prod","qty","price"
//                    )
//                    .withCommentMarker('#')
//                    .withIgnoreSurroundingSpaces()
//                    .withEscape('\\')
//                    .parse(in);
            Iterable<CSVRecord> records = CSVFormat.DEFAULT
                    .withFirstRecordAsHeader()
                    .withCommentMarker('#')
                    .withHeader(
                      "id","time","prod","qty","price"
                    )
                    .parse(in);
            for (CSVRecord record : records) {
                System.out.print(record.get("id") + ":" + record.get("time"));
                System.out.print("," + record.get("prod")+ ",");
                System.out.print(record.get("qty"));
                System.out.print("x");
                System.out.println(record.get("price"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
