package com.wiredbraincoffee.demos;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import java.util.List;

public class UserData4Slides {
    @JsonProperty("name")
    public String firstName;
    @JsonProperty("surname")
    public String lastName;
    public int totalSales;
    @JacksonXmlElementWrapper(localName = "salesAreas")
    @JacksonXmlProperty(localName = "salesArea")
    public List<Integer> salesArea;
    public boolean supervisor;
}
