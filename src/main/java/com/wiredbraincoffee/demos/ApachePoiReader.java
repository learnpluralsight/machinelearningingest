package com.wiredbraincoffee.demos;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/*
https://www.codejava.net/coding/how-to-read-excel-files-in-java-using-apache-poi
https://www.baeldung.com/apache-poi-read-cell-value-formula
 */
public class ApachePoiReader {
    public static void main(String[] args) {
        try {

            InputStream inputStream = new FileInputStream(
              new File("demo-files/purchases-june-2020.xlsx")
            );
            Workbook workbook = new XSSFWorkbook(inputStream);

            Sheet firstSheet = workbook.getSheet("Purchases");

            Cell firstCell = firstSheet.getRow(0).getCell(0);
            System.out.println(firstCell.getStringCellValue());

            for (Row nextRow : firstSheet) {
                for (Cell cell : nextRow) {
                    String output = switch (cell.getCellType()) {
                        // Excel Data Types
                        case STRING -> "S:" + cell.getStringCellValue();
                        case BOOLEAN -> "B:" + cell.getBooleanCellValue();
                        case NUMERIC -> DateUtil.isCellDateFormatted(cell) ?
                            "D:" + cell.getLocalDateTimeCellValue()
                            : "N:" + cell.getNumericCellValue();
                        case FORMULA -> "F:" + cell.getCellFormula()
                            + "(" + cell.getCachedFormulaResultType() + ")"
                            + "=" + cell.getNumericCellValue();
                        case BLANK -> "X";
                        case ERROR -> "ERR";
                        case _NONE -> "?";
                    };
                    System.out.print(output + "  ");
                }
                System.out.println();
            }
            workbook.close();
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
