package com.wiredbraincoffee.demos;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;

import java.io.File;
import java.io.IOException;

public class JacksonMapper2 {
    public static void main(String[] args) {
        try {
            ObjectMapper mapper = new JsonMapper();
            UserData userData = mapper.readValue(
              new File("demo-files/user-data.json"),
              UserData.class
            );
            System.out.println(userData);
            System.out.println(userData.getFirstName() + " " + userData.getLastName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
