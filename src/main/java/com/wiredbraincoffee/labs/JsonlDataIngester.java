package com.wiredbraincoffee.labs;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.wiredbraincoffee.ingest.SaleTransaction;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class JsonlDataIngester {
    public static void main(String[] args) {
        ObjectMapper mapper = new JsonMapper();
        try (BufferedReader reader = new BufferedReader(new FileReader("lab-files/sales-jp.jsonl"))) {
            String line;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                SaleTransaction sale = mapper.readValue(line, SaleTransaction.class);
                System.out.println(sale);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
