package com.wiredbraincoffee.ingest;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ExcelDataIngester {

    public static SaleTransaction rowToSale(Row row) {
        SaleTransaction sale = new SaleTransaction();
        if (row.getCell(0) != null) sale.setUuid(row.getCell(0).getStringCellValue());
        if (row.getCell(1) != null) sale.setTimestamp(row.getCell(1).getStringCellValue());
        if (row.getCell(2) != null) sale.setType(row.getCell(2).getStringCellValue());
        if (row.getCell(3) != null) sale.setSize(row.getCell(3).getStringCellValue());
        if (row.getCell(4) != null) sale.setPrice(row.getCell(4).getStringCellValue());
        if (row.getCell(5) != null) sale.setOffer(row.getCell(5).getStringCellValue());
        if (row.getCell(6) != null) sale.setDiscount(row.getCell(6).getStringCellValue());
        if (row.getCell(7) != null) sale.setUserId((long)row.getCell(7).getNumericCellValue());
        return sale;
    }

    public static void main(String[] args) {
        String excelFilePath = "wired-brain-coffee/salesdata-ca.xlsx";
        // Autoclosing the workbook will also close the underlying input stream
        try (Workbook workbook = new XSSFWorkbook(new FileInputStream(new File(excelFilePath)))) {
            Sheet firstSheet = workbook.getSheetAt(0);
            for (Row nextRow : firstSheet) {
                if (nextRow.getRowNum() == 0) continue; // skip the first row with titles
                System.out.println(nextRow);
                SaleTransaction saleTransaction = rowToSale(nextRow);
                System.out.println(saleTransaction);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
