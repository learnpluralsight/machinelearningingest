package com.wiredbraincoffee.ingest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class JsonDataIngester {

    public static void main(String[] args) {
        ObjectMapper mapper = new JsonMapper();
        List<SaleTransaction> saleTransactionsInJapan = null;
        try {
            saleTransactionsInJapan = mapper.readValue(new File("wired-brain-coffee/sales-jp.json"), new TypeReference<List<SaleTransaction>>() {});
            for (SaleTransaction saleTx : saleTransactionsInJapan) {
                System.out.println(saleTx);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
