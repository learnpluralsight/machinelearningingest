package com.wiredbraincoffee.ingest;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class CsvDataIngester {

    public static SaleTransaction recordToSale(CSVRecord record) {
        SaleTransaction sale = new SaleTransaction();
        sale.setUuid(record.get("txid"));
        sale.setTimestamp(record.get("txts"));
        sale.setType(record.get("coffee"));
        sale.setSize(record.get("size"));
        sale.setPrice(record.get("price"));
        sale.setDiscount(record.get("discount"));
        sale.setOffer(record.get("offer"));
        sale.setUserId(Long.parseLong(record.get("userid")));
        return sale;
    }

    public static void main(String[] args) {
        try (Reader in = new FileReader("wired-brain-coffee/salesdata-uk.csv")) {
            // CSVFormat.DEFAULT is the same as CSVFormat.RFC4180 but allows empty lines
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(in);
            for (CSVRecord record : records) {
                System.out.println(record);
                SaleTransaction saleObj = recordToSale(record);
                System.out.println(saleObj);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
