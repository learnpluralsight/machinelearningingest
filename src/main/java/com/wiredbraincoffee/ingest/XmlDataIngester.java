package com.wiredbraincoffee.ingest;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class XmlDataIngester {

    public static void main(String[] args) {
        ObjectMapper mapper = new XmlMapper();
        List<SaleTransaction> saleTransactionsInItaly = null;
        try {
            saleTransactionsInItaly = mapper.readValue(new File("wired-brain-coffee/transactions-it.xml"), new TypeReference<List<SaleTransaction>>() {});
            for (SaleTransaction saleTx : saleTransactionsInItaly) {
                System.out.println(saleTx);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
